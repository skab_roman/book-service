package com.training;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DBObject;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@DataMongoTest
@ExtendWith(SpringExtension.class)
public class EmbeddedMongoDbIntegrationTest {
    @Autowired
    MongoTemplate mongoTemplate;

    @Test
    void mongoDbIntegrationTest() {
        DBObject objectToSave = BasicDBObjectBuilder.start()
                .add("title", "Angels & Demons")
                .add("author", "Dan Brown")
                .add("genre", "Mystery thriller")
                .add("publishYear", "2000")
                .get();
        mongoTemplate.save(objectToSave, "books");

        assertThat(mongoTemplate.findAll(DBObject.class, "books"))
                .extracting("title")
                .contains("Angels & Demons");
    }
}
