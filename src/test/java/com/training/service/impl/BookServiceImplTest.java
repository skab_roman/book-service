package com.training.service.impl;

import com.training.dto.BookDto;
import com.training.model.Book;
import com.training.repository.BookRepository;
import com.training.service.mapper.BookDtoDomainMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BookServiceImplTest {
    public static final String BOOK_ID = "613e2b369144307c8a22c9bd";

    @Mock
    private BookRepository bookRepository;

    @Mock
    private BookDtoDomainMapper mapper;

    @InjectMocks
    BookServiceImpl bookService;

    @Test
    void getByIdTest(){
        Book book = new Book();
        book.setId(BOOK_ID);
        when(bookRepository.getById(BOOK_ID)).thenReturn(book);

        BookDto bookDto = new BookDto();
        bookDto.setId(BOOK_ID);
        when(mapper.mapBookToBookDto(book)).thenReturn(bookDto);

        BookDto retrievedBookDto = bookService.getById(BOOK_ID);
        assertEquals(BOOK_ID, retrievedBookDto.getId());
    }
}
