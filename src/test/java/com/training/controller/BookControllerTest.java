package com.training.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.training.dto.BookDto;
import com.training.service.BookService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(value = BookController.class)
@AutoConfigureMockMvc(addFilters = false)
public class BookControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BookService bookService;

    private final ObjectMapper mapper = new ObjectMapper();

    @Test
    void createTest() throws Exception {
        BookDto bookDto = new BookDto();
        bookDto.setTitle("Angels & Demons");
        bookDto.setAuthor("Dan Brown");
        bookDto.setGenre("Mystery thriller");
        bookDto.setPublishYear(2000);

        String bookId = "613e2b369144307c8a22c9bd";
        when(bookService.create(bookDto)).thenReturn(bookId);

        mockMvc.perform(post("/api/v1/books")
                .content(mapper.writeValueAsString(mapper.writeValueAsString(bookDto)))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }
}
