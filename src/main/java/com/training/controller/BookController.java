package com.training.controller;

import com.training.api.BookApi;
import com.training.dto.BookDto;
import com.training.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
public class BookController implements BookApi {
    private final BookService bookService;

    @Override
    public String create(BookDto bookDto) {
        log.info("creating Book: {}", bookDto);
        String id = bookService.create(bookDto);
        log.info("successfully created book with id{}", id);
        return id;
    }

    @Override
    public BookDto getById(String id) {
        log.info("getBook: with id {}", id);
        BookDto book = bookService.getById(id);
        log.info("successfully received book with id {}", id);
        return book;
    }

    @Override
    public void update(String id, BookDto bookDto) {
        log.info("updateBook: input bookDto {}", bookDto);
        bookService.update(id, bookDto);
        log.info("successfully updated book with id {}", id);
    }

    @Override
    public void delete(String id) {
        log.info("deleteBook: input book id {}", id);
        bookService.delete(id);
        log.info("successfully deleted book with id{}", id);
    }

    @Override
    public List<BookDto> get(String genre, String author, Integer publishYear, String sortField, String sortOrder) {
        log.info("get all books");
        List<BookDto> bookDtoList = bookService.get(genre, author, publishYear, sortField, sortOrder);
        log.info("successfully received {} books", bookDtoList.size());
        return bookDtoList;
    }

}
