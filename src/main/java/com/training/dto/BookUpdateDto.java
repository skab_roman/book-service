package com.training.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

@Data
@ApiModel(description = "Book model for updating")
public class BookUpdateDto {
    @NotBlank
    @ApiModelProperty(notes = "Title of the book", required = true)
    private String title;

    @NotBlank
    @ApiModelProperty(notes = "Genre of the book", required = true)
    private String genre;

    @NotBlank
    @ApiModelProperty(notes = "Author of the book", required = true)
    private String author;

    @Positive
    @ApiModelProperty(notes = "Publish year of the book", required = true, example = "1999")
    private int publishYear;
}
