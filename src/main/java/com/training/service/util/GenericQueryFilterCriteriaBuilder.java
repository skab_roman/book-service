package com.training.service.util;

import com.training.model.FilterCondition;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.ArrayList;
import java.util.List;

public class GenericQueryFilterCriteriaBuilder {
    private final List<FilterCondition> filterConditions;

    public GenericQueryFilterCriteriaBuilder() {
        filterConditions = new ArrayList<>();
    }

    public void addCondition(String field, Object value){
        if (value != null){
            filterConditions.add(new FilterCondition(field, value));
        }
    }

    public Query buildQuery() {
        if (!filterConditions.isEmpty()) {
            Query query = new Query();
            filterConditions.forEach(condition -> query.addCriteria(buildCriteria(condition)));
            return query;
        }else {
            return new Query();
        }
    }

    private Criteria buildCriteria(FilterCondition condition){
        return Criteria.where(condition.getField()).is(condition.getValue());
    }
}
