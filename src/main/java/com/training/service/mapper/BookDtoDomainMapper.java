package com.training.service.mapper;

import com.training.dto.BookDto;
import com.training.model.Book;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class BookDtoDomainMapper {

    public Book mapBookDtoToBook(BookDto bookDto){
        log.debug("mapBookDtoToBook: map to Book from BookDto: {}", bookDto);
        Book book = new Book();
        BeanUtils.copyProperties(bookDto, book);
        return book;
    }

    public BookDto mapBookToBookDto(Book book){
        BookDto bookDto = new BookDto();
        BeanUtils.copyProperties(book, bookDto);
        log.debug("mapBookToBookDto: map from Book to BookDto: {}", bookDto);
        return bookDto;
    }

}
