package com.training.service;

import com.training.dto.BookDto;

import java.util.List;

public interface BookService {
    String create(BookDto bookDto);

    BookDto getById(String id);

    void update(String id, BookDto bookDto);

    void delete(String id);

    List<BookDto> get(String genre, String author, Integer publishYear, String sortField, String sortOrder);

}
