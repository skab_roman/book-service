package com.training.service.impl;

import com.training.dto.BookDto;
import com.training.exception.BookNotFoundException;
import com.training.model.Book;
import com.training.repository.BookRepository;
import com.training.repository.ResourceRepository;
import com.training.service.util.GenericQueryFilterCriteriaBuilder;
import com.training.service.BookService;
import com.training.service.mapper.BookDtoDomainMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    public static final String FIELD_GENRE = "genre";
    public static final String FIELD_AUTHOR = "author";
    public static final String FIELD_PUBLISH_YEAR = "publishYear";
    public static final String SORT_DESC = "DESC";

    private final BookRepository bookRepository;
    private final ResourceRepository resourceRepository;
    private final BookDtoDomainMapper mapper;


    @Override
    public String create(BookDto bookDto) {
        log.info("createBook: about to create a new book {}", bookDto);
        Book book = mapper.mapBookDtoToBook(bookDto);
        book = bookRepository.save(book);
        log.info("Book with id {} successfully created", book.getId());
        return book.getId();
    }

    @Override
    public BookDto getById(String id) {
        log.info("getBook: with id {}", id);
        Book book = bookRepository.getById(id);
        if (book != null){
            BookDto bookDto = mapper.mapBookToBookDto(book);
            log.info("successfully received book with id {}", id);
            return bookDto;
        }else {
            log.warn("Book not found");
            throw new BookNotFoundException(id);
        }

    }

    @Override
    public void update(String id, BookDto bookDto) {
        log.info("updateBook: input bookDto {}", bookDto);
        Book bookById = bookRepository.getById(id);
        if (bookById == null){
            throw new BookNotFoundException(id);
        }
        bookDto.setId(id);
        Book book = mapper.mapBookDtoToBook(bookDto);
        bookRepository.save(book);
        log.info("successfully updated book with id {}", id);
    }

    @Override
    public void delete(String id) {
        log.info("About to delete book with id: {}", id);
        bookRepository.deleteById(id);
        log.info("successfully deleted book with id {}", id);
    }

    @Override
    public List<BookDto> get(String genre, String author, Integer publishYear, String sortField, String sortOrder) {
        log.info("Retrieving all books");
        GenericQueryFilterCriteriaBuilder filterCriteriaBuilder = new GenericQueryFilterCriteriaBuilder();
        filterCriteriaBuilder.addCondition(FIELD_GENRE, genre);
        filterCriteriaBuilder.addCondition(FIELD_AUTHOR, author);
        filterCriteriaBuilder.addCondition(FIELD_PUBLISH_YEAR, publishYear);
        Query query = filterCriteriaBuilder.buildQuery();
        if (sortField != null) {
            Sort sort = evaluateSort(sortField, sortOrder);
            query.with(sort);
        }
        List<Book> books = resourceRepository.findBooksByQuery(query);
        List<BookDto> bookDtoList = books.stream().map(mapper::mapBookToBookDto).collect(Collectors.toList());
        log.info("successfully received {} books", bookDtoList.size());

        return bookDtoList;
    }

    private Sort evaluateSort(String sortField, String sortOrder){
        Sort sort = Sort.by(sortField);
        if (sortOrder != null){
            sort = sortOrder.equalsIgnoreCase(SORT_DESC) ? sort.descending() : sort.ascending();
        }
        return sort;
    }
}
