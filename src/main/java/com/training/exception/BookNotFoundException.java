package com.training.exception;

public class BookNotFoundException extends RuntimeException{
    private static final String MESSAGE = "Book with id %s not found";

    public BookNotFoundException(String id) {
        super(String.format(MESSAGE, id));
    }
}
