package com.training.repository;

import com.training.model.Book;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BookRepository extends MongoRepository<Book, String> {
    Book save(Book book);

    Book getById(String id);

    void deleteById(String id);

}
