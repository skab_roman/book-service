package com.training.repository.impl;

import com.training.model.Book;
import com.training.repository.ResourceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class ResourceRepositoryImpl implements ResourceRepository {
    private final MongoTemplate mongoTemplate;

    @Override
    public List<Book> findBooksByQuery(Query query) {
        return mongoTemplate.find(query, Book.class);
    }
}
