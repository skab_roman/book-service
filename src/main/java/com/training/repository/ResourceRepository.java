package com.training.repository;

import com.training.model.Book;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public interface ResourceRepository {
    List<Book> findBooksByQuery(Query query);
}
