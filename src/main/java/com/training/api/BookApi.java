package com.training.api;

import com.training.dto.BookDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "Book REST API")
@RequestMapping("/api/v1/books")
public interface BookApi {

    @ApiOperation("Create book")
    @ApiResponse(code = 201, message = "Created", response = String.class)
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    String create(BookDto bookDto);

    @ApiOperation("Get book by id")
    @ApiResponse(code = 200, message = "OK", response = BookDto.class)
    @GetMapping(value = "{id}")
    @ResponseStatus(HttpStatus.OK)
    BookDto getById(@PathVariable String id);

    @ApiOperation("Update book")
    @ApiResponse(code = 200, message = "OK")
    @PutMapping(value = "{id}")
    @ResponseStatus(HttpStatus.OK)
    void update(@PathVariable String id, BookDto bookDto);

    @ApiOperation("Delete book by id")
    @ApiResponse(code = 202, message = "Accepted")
    @DeleteMapping(value = "{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    void delete(@PathVariable String id);

    @ApiOperation(value = "Retrieves list of books")
    @ApiResponse(code = 200, message = "OK")
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    List<BookDto> get(
            @RequestParam(value = "genre", required = false) String genre,
            @RequestParam(value = "author", required = false) String author,
            @RequestParam(value = "publishYear", required = false) Integer publishYear,
            @RequestParam(value = "sortField", required = false) String sortField,
            @RequestParam(value = "sortOrder", required = false) String sortOrder
    );
}
